#!/bin/bash
# Find out base dir
BASEDIR=$(dirname "$0")
# Put static header into motd
cat $BASEDIR/header.txt > /etc/motd
# Randomly select some ASCII-Art
ARTFILE="$(ls $BASEDIR/art |sort -R |head -n 1)"
# Append selected Artdir to motd
cat "$BASEDIR/art/$ARTFILE" >> /etc/motd
printf "\n\n" >> /etc/motd
# Append random Quote to motd
cat "$BASEDIR/quotes.txt" | sort -R | head -n 1 >> /etc/motd


